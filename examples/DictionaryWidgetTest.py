from pyqtlib import QtCore, QtGui

from pyqtlib.tree.dictionary_widget import  DictionaryWidget

import sys

from pprint import pprint



class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.widget = DictionaryWidget()
        self.setCentralWidget(self.widget)


        d = {
            'floatVal': 1.234,
            'listVal' : ['a','b','c','d'],
            #'stringVal' : 'this is a string',
            'bool' : True
        }

        self.widget.setDictionary(d)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv[1:])

    main = MainWindow()
    main.show()
    sys.exit(app.exec_())