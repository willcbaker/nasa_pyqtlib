#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import Node, BoxNode, CirclePort, RequiresPort, ProvidesPort, PortOrientation, GraphView, GraphScene
from pyqtlib.node_graph import LineConnection, SplineConnection, MultiTypeConnection, Element
from pyqtlib.node_graph.Connection import Connection
from pyqtlib.node_graph.MultiTypeConnection import Vertex

from pyqtlib.node_graph import SimpleOrthoLine

from GraphSceneViewMain import GraphSceneViewMain


class OrthoLine(Connection):

    def __init__(self, parent=None):
        super(OrthoLine, self).__init__(parent=parent)
        self.vertices = []

        self.nextVertexPos = self.scenePos()

    def addVertex(self, point=None):
        if point is None:
            mouseGlobal = QtGui.QCursor.pos()
            point = self.mapFromGlobal(mouseGlobal)

        vertex = Vertex(parent=self)
        vertex.setPos(point)
        vertex.moveable = True
        self.vertices.append(vertex)
        self.update()

    def continueEvent(self, event):
        print 'continueEvent'
        self.addVertex(self.nextVertexPos)

    @property
    def endOfPath(self):
        if self.path is not None:
            return self.path.currentPosition()
        else:
            return self.pos()

    def paint(self, painter, option, widget):

        self.path = QtGui.QPainterPath(self.startPoint)

        if len(self.vertices) > 0:
            startPoint = self.startPoint
            endPoint = self.vertices[0].connectPointInScene()

            dx = self.startPoint.x() - endPoint.x()
            dy = self.startPoint.y() - endPoint.y()

            if abs(dx) > abs(dy):
                nextPos_x = endPoint.x()
                nextPos_y = startPoint.y()
            else:
                nextPos_x = startPoint.x()
                nextPos_y = endPoint.y()

            self.path.lineTo(nextPos_x, nextPos_y)

            for vertex in self.vertices:
                self.path.lineTo(vertex.connectPointInScene())

        currentPos = self.path.currentPosition()
        endPoint = self.endPoint

        dx = currentPos.x() - endPoint.x()
        dy = currentPos.y() - endPoint.y()

        if abs(dx) > abs(dy):
            nextPos_x = endPoint.x()
            nextPos_y = currentPos.y()
        else:
            nextPos_x = currentPos.x()
            nextPos_y = endPoint.y()

        self.nextVertexPos = QtCore.QPointF(nextPos_x, nextPos_y)
        self.path.lineTo(self.nextVertexPos)
        self.path.lineTo(endPoint)
        self.setPath(self.path)
        super(OrthoLine, self).paint(painter, option, widget)


# class DiamondPath(Element):
#
#     width = 6
#     height = 8
#
#     def __init__(self, parent=None):
#         super(DiamondPath, self).__init__(parent=parent)
#
#         self._orthogonalMove = True
#
#         # storage for calculating orthogonal movement
#         self.mousePressStartPoint = self.scenePos()
#         self.posWhenMousePressed = self.scenePos()
#         self.updatePath()
#
#         self.buddies = set()
#
#     @property
#     def orthogonalMove(self):
#         return self._orthogonalMove
#
#     @orthogonalMove.setter
#     def orthogonalMove(self, value):
#         self._orthogonalMove = value
#
#     def mousePressEvent(self, event):
#         """
#         Override to capture mouse press events
#
#         Args:
#             event: QGraphicsSceneMouseEvent
#         """
#         self.mousePressStartPoint = event.scenePos()
#         self.posWhenMousePressed = self.scenePos()
#         super(DiamondPath, self).mousePressEvent(event)
#
#     def mouseMoveEvent(self, event):
#         """
#         Override to capture mouse move events
#
#         Args:
#             event: QGraphicsSceneMouseEvent
#         """
#         oldPos = self.pos()
#         if self.orthogonalMove:
#             dx = event.scenePos().x() - self.mousePressStartPoint.x()
#             dy = event.scenePos().y() - self.mousePressStartPoint.y()
#             new_x = event.scenePos().x()
#             new_y = self.posWhenMousePressed.y()
#
#             self.setPos(new_x, new_y)
#         else:
#             super(DiamondPath, self).mouseMoveEvent(event)
#
#         self.parentItem().childMoved(self, oldPos, self.pos())
#
#     def updatePath(self):
#         p = QtGui.QPainterPath()
#
#         p.moveTo(-DiamondPath.width / 2.0, 0.0)
#         p.lineTo(0.0, DiamondPath.height / 2.0)
#         p.lineTo(DiamondPath.width / 2.0, 0.0)
#         p.lineTo(0.0, -DiamondPath.height / 2.0)
#         p.lineTo(-DiamondPath.width / 2.0, 0.0)
#
#         self.setPath(p)
#         self.setPen(QtGui.QPen(QtCore.Qt.darkYellow))
#         self.setBrush(QtCore.Qt.yellow)


# class SimpleOrthoLine(Connection):
#
#     def __init__(self, parent=None):
#         super(SimpleOrthoLine, self).__init__(parent=parent)
#         self.diamondHandle = DiamondPath(parent=self)
#         self.diamondMoved = False
#         self.setAcceptHoverEvents(True)
#
#     def hoverEnterEvent(self, event):
#         self.diamondHandle.show()
#         self.update()
#
#     def hoverLeaveEvent(self, event):
#         self.diamondHandle.hide()
#         self.update()
#
#     def childMoved(self, item, oldPos, newPos):
#         self.diamondMoved = True
#
#     def paint(self, painter, option, widget):
#         startPoint = self.startPoint
#         endPoint = self.endPoint
#
#         mid_x = (endPoint.x() + startPoint.x()) / 2.0
#         mid_y = (endPoint.y() + startPoint.y()) / 2.0
#
#         if self.diamondMoved:
#             diamond_x = self.diamondHandle.pos().x()
#             diamond_y = mid_y
#         else:
#             diamond_x = mid_x
#             diamond_y = mid_y
#
#         self.diamondHandle.setPos(diamond_x, diamond_y)
#
#         self.path = QtGui.QPainterPath(startPoint)
#         self.path.lineTo(diamond_x, startPoint.y())
#         self.path.lineTo(diamond_x, endPoint.y())
#         self.path.lineTo(endPoint.x(), endPoint.y())
#         self.setPath(self.path)
#         super(SimpleOrthoLine, self).paint(painter, option, widget)



class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.lineEdit = QtGui.QLineEdit()

        self.addCirclePortButton = QtGui.QPushButton('add CirclePort')

        self.currentConnectionTypeLabel = QtGui.QLabel('LineConnection')

        self.setConnectionLineButton = QtGui.QPushButton('set Line connection')
        self.setConnectionSplineButton = QtGui.QPushButton('set Spline connection')
        self.setConnectionMultiSegmentButton = QtGui.QPushButton('set MultiSegment connection')
        self.setConnectionOrthoLineButton = QtGui.QPushButton('set ortho line connection')
        self.setConnectionSimpleOrthoLineButton = QtGui.QPushButton('set simple ortho line connection')

        self.connectToggle = QtGui.QCheckBox('connect mode')
        self.connectToggle.setChecked(False)
        self.connectToggle.stateChanged.connect(self.updateMouseMode)

        self.addCirclePortButton.clicked.connect(self.addCirclePort)

        self.setConnectionLineButton.clicked.connect(self.setConnectionLine)
        self.setConnectionSplineButton.clicked.connect(self.setConnectionSpline)
        self.setConnectionMultiSegmentButton.clicked.connect(self.setConnectionMultiSegment)
        self.setConnectionOrthoLineButton.clicked.connect(self.setConnectionOrthoLine)
        self.setConnectionSimpleOrthoLineButton.clicked.connect(self.setConnectionSimpleOrthoLine)

        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(self.connectToggle)
        #hLayout.addWidget(self.lineEdit)
        hLayout.addStretch()

        layout = self.getMainLayout()
        layout.addWidget(self.view)
        layout.addLayout(hLayout)

        layout.addWidget(self.addCirclePortButton)
        layout.addWidget(self.currentConnectionTypeLabel)
        layout.addWidget(self.setConnectionLineButton)
        layout.addWidget(self.setConnectionSplineButton)
        layout.addWidget(self.setConnectionMultiSegmentButton)
        layout.addWidget(self.setConnectionOrthoLineButton)
        layout.addWidget(self.setConnectionSimpleOrthoLineButton)

        self.count = 0
        self.node_count = 0
        self.output = True

        self.setConnectionOrthoLine()

    def addBoxNode(self):
        node = BoxNode('BoxNode', 'Node{}'.format(self.node_count))
        self.node_count += 1
        self.scene.addItem(node)

    def getSelectedNode(self):
        selectedItems = self.scene.selectedItems()
        if len(selectedItems) > 0:
            item = selectedItems[0]
            if isinstance(item,BoxNode):
                return item
        return None

    def addCirclePort(self):
        port = CirclePort('port_{}'.format(self.count))
        self.scene.addItem(port)

    def updateMouseMode(self, value):
        if self.connectToggle.isChecked():
            self.scene.setMouseModeConnect()
        else:
            self.scene.setMouseModePointer()

    def setConnectionLine(self):
        self.scene.setConnectionType(LineConnection)
        self.currentConnectionTypeLabel.setText('LineConnection')

    def setConnectionSpline(self):
        self.scene.setConnectionType(SplineConnection)
        self.currentConnectionTypeLabel.setText('SplineConnection')

    def setConnectionMultiSegment(self):
        self.scene.setConnectionType(MultiSegmentConnection)
        self.currentConnectionTypeLabel.setText('MultiSegmentConnection')

    def setConnectionOrthoLine(self):
        self.scene.setConnectionType(OrthoLine)
        self.currentConnectionTypeLabel.setText('OrthoLine')

    def setConnectionSimpleOrthoLine(self):
        self.scene.setConnectionType(SimpleOrthoLine)
        self.currentConnectionTypeLabel.setText('Simple OrthoLine')


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
