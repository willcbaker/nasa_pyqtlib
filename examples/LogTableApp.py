#!/usr/bin/env python

"""
Example app for the LogTableWidget.


"""

import time

from pyqtlib import QtCore, QtGui

from pyqtlib import LogTableWidget

level = [
    'DEBUG',
    'INFO',
    'WARNING',
    'error']

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)


        self.log_levels = sorted(LogTableWidget.LogLevels.keys())
        self.sysLog = LogTableWidget()
        self.setCentralWidget(self.sysLog)

        self.i = 0

        #Start the update timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateView)
        self.timer.start(1000)



        self.show()

    def updateView(self):
        t = time.strftime("%Y-%m-%d %H:%M:%S")
        host = 'hostname'
        src  = 'source'
        category = 'category'
        level = self.log_levels[self.i % len(self.log_levels)]
        print level
        msg = 'Test message'
        self.sysLog.addEntry(t, host, src, level, category, msg)
        self.i = self.i + 1


def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()