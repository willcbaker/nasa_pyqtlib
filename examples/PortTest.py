#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore

from GraphSceneViewMain import GraphSceneViewMain
from pyqtlib.node_graph import CirclePort, ProvidesPort, RequiresPort, PortOrientation, ArrowOutPort, ArrowInPort

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.addPortButton = QtGui.QPushButton('add port')
        self.addPortButton.clicked.connect(self.addPort)

        self.addProvidesPortButton = QtGui.QPushButton('add ProvidesPort')
        self.addProvidesPortButton.clicked.connect(self.addProvidesPort)

        self.addRequiresPortButton = QtGui.QPushButton('add RequiresPort')
        self.addRequiresPortButton.clicked.connect(self.addRequiresPort)

        self.addArrowOutPortButton = QtGui.QPushButton('add ArrowOutPort')
        self.addArrowOutPortButton.clicked.connect(self.addArrowOutPort)

        self.addArrowInPortButton = QtGui.QPushButton('add ArrowInPort')
        self.addArrowInPortButton.clicked.connect(self.addArrowInPort)

        self.outputCheckbox = QtGui.QCheckBox('output')
        self.outputCheckbox.setChecked(True)

        layout = self.getMainLayout()
        layout.addWidget(self.outputCheckbox)
        layout.addWidget(self.addPortButton)
        layout.addWidget(self.addProvidesPortButton)
        layout.addWidget(self.addRequiresPortButton)
        layout.addWidget(self.addArrowOutPortButton)
        layout.addWidget(self.addArrowInPortButton)

        self.count = 0
        self.output = True



    def addPort(self):
        item = CirclePort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1
        self.scene.addItem(item, store_undo=True)

    def addProvidesPort(self):
        item = ProvidesPort('port_{}'.format(self.count), orientation=self.getOrientation())
        self.count += 1
        self.scene.addItem(item, store_undo=True)

    def addRequiresPort(self):
        item = RequiresPort('port_{}'.format(self.count), orientation=self.getOrientation())
        self.count += 1
        self.scene.addItem(item, store_undo=True)

    def addArrowOutPort(self):
        item = ArrowOutPort('port_{}'.format(self.count), orientation=self.getOrientation())
        self.count += 1
        self.scene.addItem(item, store_undo=True)

    def addArrowInPort(self):
        item = ArrowInPort('port_{}'.format(self.count), orientation=self.getOrientation())
        self.count += 1
        self.scene.addItem(item, store_undo=True)

    def getOrientation(self):
        if self.outputCheckbox.isChecked():
            return PortOrientation.Right
        else:
            return PortOrientation.Left


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    ex.resize(1200,600)
    sys.exit(app.exec_())
