from pyqtlib import QtGui, QtCore

from pyqtlib import TreeFilterWidget

from pyqtlib import PathModel

import sys

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.model = PathModel()
        self.view = TreeFilterWidget()

        self.view.setModel(self.model)

        self.model.addPath('/usr/local/bin')
        self.model.addPath('/usr/local/lib')

        self.setCentralWidget(self.view)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    ex.resize(1200,600)
    sys.exit(app.exec_())