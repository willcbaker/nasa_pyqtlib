#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import BoxNode, CirclePort, RequiresPort, ProvidesPort, PortOrientation, LineConnection, SplineConnection, ArrowOutPort, ArrowInPort
from GraphSceneViewMain import GraphSceneViewMain

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.node1 = BoxNode('Node', 'Node 1')
        self.node2 = BoxNode('Node', 'Node 2')
        self.scene.addItem(self.node1)
        self.scene.addItem(self.node2)

        self.node1.addPort(ProvidesPort('provides1',orientation=PortOrientation.Right))
        self.node1.addPort(CirclePort('provides2', orientation=PortOrientation.Right))
        self.node1.addPort(ArrowOutPort('provides3', orientation=PortOrientation.Right))
        self.node2.addPort(RequiresPort('requires1',orientation=PortOrientation.Left))
        self.node2.addPort(CirclePort('requires2', orientation=PortOrientation.Left))
        self.node2.addPort(ArrowInPort('requires3', orientation=PortOrientation.Left))

        self.node2.setPos(150.0, 0.0)

        self.conn1 = LineConnection()
        self.conn2 = LineConnection()
        self.conn3 = LineConnection()
        self.conn1.connect(self.node1.getPort('provides1'), self.node2.getPort('requires1'))
        # self.conn2.connect(self.node1.getPort('provides2'), self.node2.getPort('requires2'))
        # self.conn3.connect(self.node1.getPort('provides3'), self.node2.getPort('requires3'))
        self.scene.addItem(self.conn1)
        self.scene.addItem(self.conn2)
        self.scene.addItem(self.conn3)

        self.count = 0
        self.output = True


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
