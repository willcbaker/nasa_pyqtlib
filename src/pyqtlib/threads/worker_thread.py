from pyqtlib import QtCore, QtGui

class WorkerThread(QtCore.QThread):
    """
    Generic worker thread.

    This method can be used to create a generic worker thread.  The user passes a callable and a list
    of arguments.  When the thread is started, the run method will execute the callable and pass in the arguments.
    After completing the call, the return value will be emitted in the "workerDone" signal.

    Example: (NOTE: example has not been testing for correctness.  Just trying to show the idea)

    class MyWidget(QtGui.QWidget):

        def refresh(self):
            # NOTE: In order for the threading to be effective,
            # you must make the worker instance a member of the class.
            self.worker = WorkerThread(someOtherMethod)
            self.worker.workerDone.connect(self._refreshComplete)
            self.worker.start()

        def _refreshComplete(self, otherMethodReturnValue):
            self.do_something(otherMethodReturnValue)
    """

    workerDone = QtCore.pyqtSignal(object)

    def __init__(self, function, *args, **kwargs):
        QtCore.QThread.__init__(self)
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def __del__(self):
        self.wait()

    def run(self):
        val = self.function(*self.args, **self.kwargs)
        self.workerDone.emit(val)
