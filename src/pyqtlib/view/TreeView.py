from pyqtlib import QtGui, QtCore


class TreeView(QtGui.QTreeView):

    def __init__(self, parent=None):
        super(TreeView, self).__init__(parent)

        # Actions
        self.expandAllAction = QtGui.QAction('&Expand All', self, triggered=self.expandAll)
        self.collapseAllAction = QtGui.QAction('&Collapse All', self, triggered=self.collapseAll)
        self.adjustColumnsAction = QtGui.QAction('&Adjust Columns', self, triggered=self.adjustColumns)
        self.viewAllAction = QtGui.QAction('&View All', self, triggered=self.viewAll)

        self.button = QtGui.QPushButton('button')
        self.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)

    @QtCore.pyqtSlot()
    def adjustColumns(self):
        """
        Resize the columns to fix the data.
        """
        if self.model() is not None:
            for col in range(self.model().columnCount()):
                self.resizeColumnToContents(col)

    def contextMenuEvent(self, event):
        """
        ContextMenuEvent
        Args:
            event:

        Returns:

        """
        menu = self.getContextMenu(event)
        menu.exec_(event.globalPos())

    def getContextMenu(self, event):
        """
        Get this view's context menu

        Args:
            event: Event which generated the context menu

        Returns:
            QMenu
        """
        menu = QtGui.QMenu('View', self)
        menu.addAction(self.expandAllAction)
        menu.addAction(self.collapseAllAction)
        menu.addAction(self.adjustColumnsAction)
        menu.addAction(self.viewAllAction)
        return menu

    def getTreeWidthHint(self):
        """
        Returns:
            Width hint as an int in pixels
        """
        width = 0
        if self.model() is not None:
            for col in range(self.model().columnCount()):
                width = width + self.sizeHintForColumn(col)
        return width

    @QtCore.pyqtSlot()
    def mousePressEvent(self, event):
        """
        Override of the base mousePressEvent.

        This allows a user to "deselect" any currently selected item.

        Args:
            event: QMouseEvent
        """
        self.clearSelection()
        QtGui.QTreeView.mousePressEvent(self, event)

    def getExpandedStates(self):
        """
        Get the expanded state of all items in the tree

        Returns:
            Dictionary of {persistent model idx : bool }
        """
        state = {}
        model = self.model()
        if model is not None:
            for row in range(model.rowCount()):
                idx = model.index(row, 0)
                state[idx] = self.isExpanded(idx)
        return state

    def setExpandedStates(self, values):
        """
        Set the expanded state of all items in the tree.

        This dictionary should be obtained from getExpandedStates()

        Args:
            values: Dictionary of {persistent model idx : bool }
        """
        model = self.model()

        if model is None:
            return

        for idx, expanded in values.items():
            item = model.itemFromIndex(idx)
            if item is not None:
                self.setExpanded(idx, expanded)

    @QtCore.pyqtSlot()
    def sizeHint(self):
        """
        Overload of the base sizeHint for the widget
        """
        size = QtGui.QTreeView.sizeHint(self)
        size.setWidth(self.getTreeWidthHint())
        return size

    @QtCore.pyqtSlot()
    def viewAll(self):
        """
        Expand the tree and adjust the columns
        """
        self.expandAll()
        self.adjustColumns()
