from pyqtlib import QtGui, QtCore
from pyqtlib.view.TreeView import TreeView
from pyqtlib.model.dictionary_model import DictionaryModel


class DictionaryView(TreeView):

    addItemRequest    = QtCore.pyqtSignal(QtCore.QModelIndex)
    removeItemRequest = QtCore.pyqtSignal(QtCore.QModelIndex)

    def __init__(self, parent=None):
        super(DictionaryView, self).__init__(parent)
        self.cachedColumnWidths = []

    def contextMenuEvent(self, event):
        """
        Context menu override for this view

        Args:
            event: context menu event as a QContextMenuEvent
        """
        menu = QtGui.QMenu()
        addItem    = menu.addAction('Add Item')
        removeItem = menu.addAction('Remove Item')
        menu.addSeparator()

        # Get the other menu options from the parent TreeView class
        sub_menu = self.getContextMenu(event)
        for action in sub_menu.actions():
            menu.addAction(action)

        # determine the users choice and the model index of the cursor
        choice = menu.exec_(QtGui.QCursor.pos())
        index = self.indexAt(event.pos())

        # call the appropriate callback
        if choice == addItem:
            self.addItemRequest.emit(index)
        elif choice == removeItem:
            self.removeItemRequest.emit(index)

    @QtCore.pyqtSlot()
    def cacheColumnWidths(self):
        columnWidths = []
        if self.model():
            root = self.model().invisibleRootItem()
            for i in range(self.model().columnCount(root.index())):
                columnWidths.append(self.columnWidth(i))
            self.cachedColumnWidths = columnWidths

    def reset(self):
        """
        Overload of QAbstractItemView to include adjusting columns
        """
        super(TreeView, self).reset()

        for col, width in enumerate(self.cachedColumnWidths):
            self.setColumnWidth(col, width)

    def setModel(self, model):
        """
        Set the model for this view

        Args:
            model: DictionaryModel

        Raises:
            TypeError: when the model passed in is not of type DictionaryModel

        """
        if not isinstance(model, DictionaryModel):
            raise TypeError('Trying to set model which is not of type DictionaryModel: "{}"'.format(type(model)))

        model.dictionaryRefreshed.connect(self.restoreCachedExpandedState)

        # connect the QTreeView signals when the tree is expanded or collapsed to inform the model
        self.expanded.connect(model.setItemExpanded)
        self.collapsed.connect(model.setItemCollapsed)
        super(DictionaryView, self).setModel(model)


    @QtCore.pyqtSlot()
    def restoreCachedExpandedState(self):
        """
        Restore the cached expanded state of the view
        """
        model = self.model()
        if model is not None:
            item = model.invisibleRootItem()
            cachedState = model.getCachedExpandedStates()
            self._restoreCachedExpandedState(item, cachedState)

    def _restoreCachedExpandedState(self, parent, state):
        """
        Restore the cached expanded state of the view

        This is the recursive implementation of the public "restoreCachedExpandedState" method.

        Args:
            parent: parent item
            state: cached expanded state as a tuple of (bool, children's expansion state)
        """
        for itemName, (expanded, children) in state.items():
            for row in range(parent.rowCount()):
                item = parent.child(row, 0)
                if item.data(QtCore.Qt.DisplayRole) == itemName:
                    idx = item.index()
                    self.setExpanded(idx, expanded)
                    self._restoreCachedExpandedState(item, children)
