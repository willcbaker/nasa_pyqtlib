from pyqtlib import QtGui, QtCore

class PathManagerWidget(QtGui.QWidget):
	"""
	The PathManagerWidget is a widget for managing a set of paths.  The widget consists
	of a list view and two buttons: 'Add path', and 'Remove path'.  When pressing 'Add path',
	the user will be prompted to select a file or directory from a file browser.  When the user
	clicks 'Remove path', the current selected path in the list view will be removed.  The list
	of files can be retrieved from the 'getPaths' method.

	This widget has two signals:

	pathAdded(str) - Emitted when a path is added.  The argument is the name of the path added
	pathRemoved(str) - Emitted when a path is removed.  The argument is the name of the path removed
	"""
	pathAdded   = QtCore.pyqtSignal(str)
	pathRemoved = QtCore.pyqtSignal(str)
	
	def __init__(self, paths=[], filters=[], dirs=False, parent=None):
		"""
		@type paths list-of-strings
		@param paths List of paths to add to the list

		@type filters list-of-strings
		@param filters List of file filters to pass to the OpenFileDialog

		@type dirs bool
		@param dirs Flag to open files or directories
		"""
		super(PathManagerWidget, self).__init__()

		self.dirs = dirs
		self.filters  = filters

		if len(filters) > 0:
			self.filter_string = '({})'.format(' '.join(filters))
		else:
			self.filter_string = '(*)'

		self.addPathButton    = QtGui.QPushButton('Add path')
		self.removePathButton = QtGui.QPushButton('Remove path')
		self.listWidget       = QtGui.QListWidget()
		self.addPathButton.clicked.connect(self.addPathDialog)
		self.removePathButton.clicked.connect(self.removeSelectedPath)

		self.addPaths(paths)

		self.layout = QtGui.QVBoxLayout()
		self.layout.addWidget(self.listWidget)
		self.layout.addWidget(self.addPathButton)
		self.layout.addWidget(self.removePathButton)
		self.setLayout(self.layout)
		self.show()

	def addPath(self, path):
		if path != None:
			if len(self.listWidget.findItems(path,QtCore.Qt.MatchExactly)) == 0:
				self.listWidget.addItem(path)
				self.listWidget.setMinimumWidth(self.listWidget.sizeHintForColumn(0))
				self.pathAdded.emit(path)

	def addPathDialog(self):
		if self.dirs:
			path = QtGui.QFileDialog.getExistingDirectory()
		else:
			path = QtGui.QFileDialog.getOpenFileName(self, caption='Select File', filter=self.filter_string)
		if path != None:
			self.addPath(str(path))

	def addPaths(self, paths):
		if paths != None:
			for path in paths:
				self.addPath(path)

	def getPaths(self):
		paths = []		
		for i in range(self.listWidget.count()):
			paths.append(str(self.listWidget.item(i).text()))
		return paths

	def removePath(self, path):
		items = self.listWidget.findItems(path,QtCore.Qt.MatchExactly)
		for item in items:
			self.listWidget.takeItem(item)
			del item

	def removeSelectedPath(self):
		item = self.listWidget.takeItem(self.listWidget.currentRow())
		del item
