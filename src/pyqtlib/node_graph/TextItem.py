from pyqtlib import QtGui, QtCore

DEFAULT_TEXT_COLOR = QtCore.Qt.black

class TextItem(QtGui.QGraphicsTextItem):

    textEdited = QtCore.pyqtSignal(str, str)

    def __init__(self, text, pos=(0,0), width=None, height=None, color=DEFAULT_TEXT_COLOR, parent=None):
        super(TextItem, self).__init__(text, parent=parent)

        self.setDefaultTextColor(color)
        flags = QtGui.QGraphicsItem.ItemIsMovable | \
                QtGui.QGraphicsItem.ItemIsSelectable | \
                QtGui.QGraphicsItem.ItemIsFocusable
        self.setFlags(flags)

        self.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.previousText = self.text()

        self.deleteable = True

        # draw options
        textOp = self.document().defaultTextOption()
        self.setPos(*pos)


    def isDeleteable(self):
        return self.deleteable

    def focusInEvent(self, event):
        """
        Overload the focusInEvent to capture the value of the text before editting
        :param event:
        :return:
        """
        self.preFocusText = self.text()
        super(TextItem, self).focusInEvent(event)

    def focusOutEvent(self, event):
        """
        Overload the focusOutEvent to emit a textEdited signal when the text leaves focus.

        Args:
            event: event
        """
        super(TextItem, self).focusOutEvent(event)
        postFocusText = self.text()

        if postFocusText != self.preFocusText:
            self.textEdited.emit(self.preFocusText, postFocusText)

    def keyPressEvent(self, event):
        if event.key() in [QtCore.Qt.Key_Enter,QtCore.Qt.Key_Return]:
            self.clearFocus()
            return
        super(TextItem, self).keyPressEvent(event)


    def setDeleteable(self, deleteable):
        """
        Set whether or not this item can be deleted

        Args:
            deleteable: bool

        Returns:

        """
        self.deleteable = deleteable

    def setEditable(self, editable):
        """
        Set whether or not the user can edit the text.

        Args:
            editable: bool
        """
        if editable:
            self.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        else:
            self.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)

    def setText(self, value):
        """
        Set the value of the text

        Args:
            value: new value
        """
        self.setPlainText(value)

    def text(self):
        """
        Get the text

        Returns:
            Value of the text as a string
        """
        return str(self.toPlainText())
