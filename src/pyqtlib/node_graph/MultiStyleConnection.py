from pyqtlib import QtGui, QtCore

from pyqtlib.node_graph import Element
from pyqtlib.node_graph.Connection import Connection


class DiamondHandle(Element):
    """
    The DiamondHandle is a small diamond that can be used as a handle for orthogonal movement.  When the orthogonalMove
    property is set to True, this Element will only move along the x direction when dragged during a mouse move event.
    """

    width = 12
    height = 16

    def __init__(self, parent=None):
        super(DiamondHandle, self).__init__(parent=parent)

        self._orthogonalMove = True
        self._penColor = QtCore.Qt.darkYellow
        self._brushColor = QtCore.Qt.yellow

        # storage for calculating orthogonal movement
        self.mousePressStartPoint = self.scenePos()
        self.posWhenMousePressed = self.scenePos()
        self.updatePath()
        self.buddies = set()

    @property
    def orthogonalMove(self):
        return self._orthogonalMove

    @orthogonalMove.setter
    def orthogonalMove(self, value):
        self._orthogonalMove = value

    @property
    def penColor(self):
        return self._penColor

    @penColor.setter
    def penColor(self, value):
        self._penColor = value
        self.updatePath()

    @property
    def brushColor(self):
        return self._brushColor

    @brushColor.setter
    def brushColor(self, value):
        self._brushColor = value
        self.updatePath()

    def mousePressEvent(self, event):
        """
        Override to capture mouse press events

        Args:
            event: QGraphicsSceneMouseEvent
        """
        self.mousePressStartPoint = event.scenePos()
        self.posWhenMousePressed = self.scenePos()
        super(DiamondHandle, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        """
        Override to capture mouse move events

        Args:
            event: QGraphicsSceneMouseEvent
        """
        oldPos = self.pos()
        if self.orthogonalMove:
            dx = event.scenePos().x() - self.mousePressStartPoint.x()
            dy = event.scenePos().y() - self.mousePressStartPoint.y()
            new_x = event.scenePos().x()
            new_y = self.posWhenMousePressed.y()

            self.setPos(new_x, new_y)
        else:
            super(DiamondHandle, self).mouseMoveEvent(event)

        self.parentItem().childMoved(self, oldPos, self.pos())

    def updatePath(self):
        """
        Draw the path, which is a yellow diamond
        """
        p = QtGui.QPainterPath()

        p.moveTo(-DiamondHandle.width / 2.0, 0.0)
        p.lineTo(0.0, DiamondHandle.height / 2.0)
        p.lineTo(DiamondHandle.width / 2.0, 0.0)
        p.lineTo(0.0, -DiamondHandle.height / 2.0)
        p.lineTo(-DiamondHandle.width / 2.0, 0.0)

        self.setPath(p)
        self.setPen(QtGui.QPen(self.penColor))
        self.setBrush(self.brushColor)


class MultiStyleConnection(Connection):
    """
    The MultiStyleConnection connection has several styles for connecting two points:

     Line:      A straight line from the startPoint to the endPoint

     Spline:    A cubic curve between the startPoint and the endPoint

     OrthoLine: Draws three orthogonal line segments between the startPoint and the endPoint.  The first segment is
                always horizontal, the second line is vertical, and the third segment is horizontal.  By default, the
                verticle section will be half-way between the startPoint's and endPoint's x coordinate.  Once the
                vertical handle has moved, however, the vertical position is saved.  By calling resetHandle, the handle
                will track the center-point again.

    The line type can be changed on-the-fly by calling the setType() method with the desired type.  The position of the
    handle will be saved even after changing the type.
    """

    class ConnectionStyle(object):
        Line = 10
        Spline = 20
        OrthoLine = 30

    def __init__(self, style=ConnectionStyle.Line, parent=None):
        super(MultiStyleConnection, self).__init__(parent=parent)
        self.orthoLineHandle = DiamondHandle(parent=self)
        self.orthoHandleMoved = False

        self.spline_c1 = DiamondHandle(parent=self)
        self.spline_c1.orthogonalMove = False
        self.spline_c1.hide()
        self.spline_c1.brushColor = QtCore.Qt.green
        self.spline_c1.penColor = QtCore.Qt.darkGreen

        self.spline_c2 = DiamondHandle(parent=self)
        self.spline_c2.orthogonalMove = False
        self.spline_c2.hide()
        self.spline_c2.brushColor = QtCore.Qt.red
        self.spline_c2.penColor = QtCore.Qt.darkRed
        self.splineHandleMoved = False

        self.handleMoved = False

        self.setStyle(style)
        self.selectable = True

    def shape(self):
        if self.path() is None:
            return super(MultiStyleConnection, self).shape()
        else:
            stroker = QtGui.QPainterPathStroker()
            stroker.setWidth(10)
            path = stroker.createStroke(self.path())
        return path

    def hoverEnterEvent(self, event):
        """
        Overload to handle hover events.  This is used to show the orthoLineHandle.

        Args:
            event: QGraphicsSceneHoverEvent
        """
        self.orthoLineHandle.show()
        self.update()

    def hoverLeaveEvent(self, event):
        """
        Overload to handle hover events.  This is used to hide the orthoLineHandle.

        Args:
            event: QGraphicsSceneHoverEvent
        """
        self.orthoLineHandle.hide()
        self.update()

    def mouseDoubleClickEvent(self, event):
        if self.style == MultiStyleConnection.ConnectionStyle.Spline:
            if self.spline_c1.isVisible():
                self.spline_c1.hide()
                self.spline_c2.hide()
            else:
                self.spline_c1.show()
                self.spline_c2.show()
        event.accept()
        return

    def childMoved(self, item, oldPos, newPos):
        """
        Callback to let this item know the diamond has moved.

        Args:
            item: item that moved
            oldPos: position before the move
            newPos: position after the move
        """
        if item == self.orthoLineHandle:
            self.orthoHandleMoved = True
        elif item == self.spline_c1 or item == self.spline_c2:
            self.splineHandleMoved = True

    def deserialize(self, definition):
        """
        Deserialize this object.  Useful when constructing the object from a file

        Args:
            definition: python dictionary
        """
        self.setStyle(definition.get('style', MultiStyleConnection.ConnectionStyle.Line))
        self.orthoHandleMoved = definition.get('orthoHandleMoved', False)
        if self.orthoHandleMoved:
            orthoPos = definition.get('orthoHandlePos', None)
            if orthoPos is None:
                self.orthoHandleMoved = False
            else:
                self.orthoLineHandle.setPos(QtCore.QPointF(*orthoPos))
        self.splineHandleMoved = definition.get('splineHandleMoved', False)
        if self.splineHandleMoved:
            c1Pos = definition.get('splineC1Pos', None)
            c2Pos = definition.get('splineC2Pos', None)
            if c1Pos is None or c2Pos is None:
                self.splineHandleMoved = False
            else:
                self.spline_c1.setPos(QtCore.QPointF(*c1Pos))
                self.spline_c2.setPos(QtCore.QPointF(*c2Pos))
        self.update()

    def resetHandles(self):
        """
        Reset the diamond handles to their default behavior
        """
        self.splineHandleMoved = False
        self.orthoHandleMoved = False
        self.update()

    def contextMenuEvent(self, event):
        """
        Override to handle context menu events.

        Args:
            event: QGraphicsSceneContextMenuEvent
        """
        menu = self.getContextMenu(event)
        if menu is not None:
            menu.exec_(event.screenPos())

    def getContextMenu(self, event):
        """
        Create and return a context menu for this item.

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:
            QMenu object populated with actions.
        """
        menu = QtGui.QMenu()

        changeToLineAction = menu.addAction('Change to LineConnection')
        changeToSplineAction = menu.addAction('Change to SplineConnection')
        changeToOrthoLineAction = menu.addAction('Change to OrthonLine')
        resetHandlesAction = menu.addAction('Reset Handles')

        changeToLineAction.triggered.connect(lambda: self.setStyle(MultiStyleConnection.ConnectionStyle.Line))
        changeToSplineAction.triggered.connect(lambda: self.setStyle(MultiStyleConnection.ConnectionStyle.Spline))
        changeToOrthoLineAction.triggered.connect(lambda: self.setStyle(MultiStyleConnection.ConnectionStyle.OrthoLine))
        resetHandlesAction.triggered.connect(lambda: self.resetHandles())
        return menu

    def paint(self, painter, option, widget):
        """
        Override to paint this object.

        Args:
            painter: QPainter
            option: QStyleOptionGraphicsItem
            widget: QWidget
        """
        path = None
        if self.style == MultiStyleConnection.ConnectionStyle.Line:
            path = self.linePath()
        elif self.style == MultiStyleConnection.ConnectionStyle.Spline:
            path = self.splinePath()
        elif self.style == MultiStyleConnection.ConnectionStyle.OrthoLine:
            path = self.orthoLinePath()

        self.setPath(path)
        super(MultiStyleConnection, self).paint(painter, option, widget)

    def orthoLinePath(self):
        """ Paint the path as an orthogonal set of lines."""
        startPoint = self.startPoint
        endPoint = self.endPoint

        mid_x = (endPoint.x() + startPoint.x()) / 2.0
        mid_y = (endPoint.y() + startPoint.y()) / 2.0

        if self.orthoHandleMoved:
            diamond_x = self.orthoLineHandle.pos().x()
            diamond_y = mid_y
        else:
            diamond_x = mid_x
            diamond_y = mid_y

        self.orthoLineHandle.setPos(diamond_x, diamond_y)

        path = QtGui.QPainterPath(startPoint)
        path.lineTo(diamond_x, startPoint.y())
        path.lineTo(diamond_x, endPoint.y())
        path.lineTo(endPoint.x(), endPoint.y())
        return path

    def linePath(self):
        """Paint the path as a straight line"""
        startPoint = self.startPoint
        endPoint = self.endPoint
        path = QtGui.QPainterPath(startPoint)
        path.lineTo(endPoint)
        return path

    def splinePath(self):
        """Paint the path as a cubic spline"""
        startPoint = self.startPoint
        endPoint = self.endPoint
        x1 = startPoint.x()
        y1 = startPoint.y()
        x2 = endPoint.x()
        y2 = endPoint.y()

        if not self.splineHandleMoved:
            dx = x2 - x1
            dy = y2 - y1
            self.spline_c1.setPos(x2, y1)
            self.spline_c2.setPos(x1, y2)
        # fx = 1
        # fy = 0
        # dx = abs(x2 - x1) * fx
        # dy = abs(y2 - y1) * fy
        # cx1 = x1 + dx
        # cy1 = y1 + dy
        # cx2 = x2 - dx
        # cy2 = y2 - dy

        cx1 = self.spline_c1.x()
        cy1 = self.spline_c1.y()

        cx2 = self.spline_c2.x()
        cy2 = self.spline_c2.y()

        path = QtGui.QPainterPath(startPoint)
        path.cubicTo(cx1, cy1, cx2, cy2, x2, y2)
        #path.quadTo(cx1, cy1, x2, y2)
        return path

    def _calcBezierMidpoint(self, z0, z1, c0, c1):
        """

        Args:
            z0: start point on the curve
            z1: end point on the curve
            c0: control point 1
            c1: control point 2

        Returns:
            position of the midpoint of the curve
        """

        # http://blog.sklambert.com/finding-the-control-points-of-a-bezier-curve/

        m0 = (z0 + c0) / 2.0
        m1 = (c0 + c1) / 2.0
        m2 = (c1 + z1) / 2.0
        m3 = (m0 + m1) / 2.0
        m4 = (m1 + m2) / 2.0
        m5 = (m3 + m4) / 2.0
        return m5

    def getPathMidpoint(self):
        mid = QtCore.QPointF(0.0, 0.0)
        if self.style == MultiStyleConnection.ConnectionStyle.Line:
            mid = (self.startPoint + self.endPoint) / 2.0
        elif self.style == MultiStyleConnection.ConnectionStyle.OrthoLine:
            mid = self.orthoLineHandle.pos()
        elif self.style == MultiStyleConnection.ConnectionStyle.Spline:
            mid = self._calcBezierMidpoint(self.startPoint, self.endPoint, self.spline_c1.pos(), self.spline_c2.pos())

        return mid

    def serialize(self):
        """
        Return a dictionary of values to reconstruct this object.

        Passing the dictionary returned by this method to deserialize() will put this object into that state.

        Returns:
            Python dictionary
        """
        definition = super(MultiStyleConnection, self).serialize()
        definition['style'] = self.style
        definition['orthoHandleMoved'] = self.orthoHandleMoved
        definition['orthoHandlePos'] = [self.orthoLineHandle.pos().x(),
                                        self.orthoLineHandle.pos().y()]
        definition['splineHandleMoved'] = self.splineHandleMoved
        definition['splineC1Pos'] = [self.spline_c1.pos().x(),
                                     self.spline_c1.pos().y()]
        definition['splineC2Pos'] = [self.spline_c2.pos().x(),
                                     self.spline_c2.pos().y()]
        return definition

    def setOrthoHandlePos(self, point):
        """
        Set the position of the diamond handle.

        Args:
            point: position as a QtCore.QPointF
        """
        self.handleMoved = True
        self.orthoLineHandle.setPos(point)

    def setStyle(self, style):
        self.style = style

        if self.style == MultiStyleConnection.ConnectionStyle.Line:
            self.orthoLineHandle.hide()
            self.spline_c1.hide()
            self.spline_c2.hide()
            self.setAcceptHoverEvents(False)
        elif self.style == MultiStyleConnection.ConnectionStyle.Spline:
            self.orthoLineHandle.hide()
            self.setAcceptHoverEvents(False)
        elif self.style == MultiStyleConnection.ConnectionStyle.OrthoLine:
            self.orthoLineHandle.hide()
            self.spline_c1.hide()
            self.spline_c2.hide()
            self.setAcceptHoverEvents(True)

