from pyqtlib import QtGui, QtCore

from pyqtlib.model import AnyChildFilterProxyModel
from pyqtlib.model import PathModel, PathItem

class PathView(QtGui.QTreeView):

    def mousePressEvent(self, event):
        """
        Override of the base mousePressEvent.

        This allows a user to "deselect" any currently selected item.

        Args:
            event: QMouseEvent
        """
        self.clearSelection()
        QtGui.QTreeView.mousePressEvent(self, event)

class PathWidget(QtGui.QWidget):
    """
    The PathWidget provides a TreeView display while accessing data in a table-like fashion.

    Hierachical references are expressed similar to paths on a file system.  Each reference
    can then have one or more values associated with it, where each value is presented on 
    the same row, but in different columns.  This approach is similar to a hierachical table.

    Values can then be set or retrieved by using the 'getValue' and 'setValue' methods.  If
    a reference (the first column, which contains the 'path') does not exist, the items and 
    rows will be created for you.
    
    Example:
        pathWidget = PathWidget()
        
        pathWidget.setData('/level1/level2/a', 'type', 'int', format='str')
        pathWidget.setData('/level1/level2/a', 'data', 'int', format='int')

        pathWidget.setData('/level1/level2/a', 'data', 'int', format='int')
        pathWidget.setData('/level1/level2/a', 'data', 'int', format='int')

    would display:

    path              | type  | data 
    ---------------------------------
    /level1           |       |
        /level2       |       |
            /a        | int   | 1234
            /level3   |       |
                /b    | float | 1.234

    

    By setting "checkable" to True in the constructor, all items in the list are now checkable.
    If an item is checked, it will also recursively check all child items.  To add a new item to
    the tree, without any values, use the addPath() method.

    This Widget has two signals:

        pathCheckChanged(path, QtCore.Qt.CheckState)
            Gets emitted when an item is checked or unchecked.  The first argument is the string of the
            path item that was checked/unchecked and the second argument is the new checkstate.  When an
            item is checked/unchecked all of it's children will also become checked/unchecked which will
            cause another signal to get emitted.

        selectionChanged(path)
            Gets emitted when the current selected item has changed.  The argument is the path of the 
            newly selected item.
    """

    pathCheckChanged = QtCore.pyqtSignal(str, QtCore.Qt.CheckState)
    pathValueChanged = QtCore.pyqtSignal(str)
    selectionChanged = QtCore.pyqtSignal(str)
    pathDoubleClicked = QtCore.pyqtSignal(str)

    def __init__(self, checkable=False, delimiters='/', parent=None):
        QtGui.QWidget.__init__(self, parent)

        # Proxy model, used for filtering
        self.proxyModel = AnyChildFilterProxyModel()
        self.proxyModel.setFilterRole(PathModel.PathRole)
        self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self.model = None

        defaultModel = PathModel(delimiters=delimiters, checkable=checkable)
        self.setModel(defaultModel)

        self.view  = PathView()
        self.view.setModel(self.proxyModel)
        self.view.setAlternatingRowColors(True)
        self.view.setDragEnabled(True)

        self.view.doubleClicked.connect(self.emitPathDoubleClicked)

        # Filter box and case toggle
        self.filterLineEdit = QtGui.QLineEdit()
        self.filterLineEdit.textChanged.connect(self.filterTextChanged)
        self.caseToggle = QtGui.QPushButton('Aa')
        self.caseToggle.setStyleSheet('padding: 3px 3px')  # set vertical and horizontal padding
        self.caseToggle.setCheckable(True)
        self.caseToggle.toggled.connect(self.setFilterCaseSensitive)
        self.caseToggle.setChecked(True)
        self.caseToggle.setToolTip('Toggle case sensitivity')

        # Actions
        self.expandAllAction = QtGui.QAction('&Expand All',self)
        self.collapseAllAction = QtGui.QAction('&Collapse All',self)
        self.adjustColumnsAction = QtGui.QAction('&Adjust Columns',self)
        self.viewAllAction = QtGui.QAction('&View All',self)

        self.expandAllAction.triggered.connect(self.expandAll)
        self.collapseAllAction.triggered.connect(self.collapseAll)
        self.adjustColumnsAction.triggered.connect(self.adjustColumns)
        self.viewAllAction.triggered.connect(self.viewAll)

        #Right-click menu
        self.contextMenu = QtGui.QMenu(self)
        self.contextMenu.addAction(self.expandAllAction)
        self.contextMenu.addAction(self.collapseAllAction)
        self.contextMenu.addAction(self.adjustColumnsAction)
        self.contextMenu.addAction(self.viewAllAction)

        # Set the context menu policy for the right-click pop-up menu
        self.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)

        # Connect model's itemChanged signal to internal method
        #self.model.itemChanged.connect(self._itemChanged)

        self.pathCheckChanged.connect(self._checkStateChanged)

        self.view.selectionModel().currentRowChanged.connect(self.emitSelectionChanged)

        # Widget layout
        self.filterLayout = QtGui.QHBoxLayout()
        self.filterLayout.addWidget(self.filterLineEdit)
        self.filterLayout.addWidget(self.caseToggle)

        self.layout = QtGui.QVBoxLayout()
        self.layout.addLayout(self.filterLayout)
        self.layout.addWidget(self.view)
        self.setLayout(self.layout)

    @QtCore.pyqtSlot(str)
    def addPath(self, path):
        """
        Add a single path to the tree

        @type path string
        @param path Path to add
        """
        self.model.addPath(path)

    @QtCore.pyqtSlot(list)
    def addPaths(self, paths):
        """
        Add a list of paths to the tree

        @type path list
        @param path list of strings to add
        """
        for path in paths:
            self.addPath(path)

    def adjustColumns(self):
        """
        Resize the columns to fit the data
        """
        for col in range(self.model.columnCount()):
            self.view.resizeColumnToContents(col)

    def clearAll(self):
        """
        Remove all data and headers from the tree
        """
        self.model.clearAll()

    def clearData(self):
        """
        Remove all data from the tree, but leave the headers
        """
        self.model.clearData()

    def collapseAll(self):
        """
        Collapse the tree
        """
        self.view.collapseAll()

    def contextMenuEvent(self,event):
        """
        This is the right-click pop-up menu actions

        @type event QEvent
        @param event The event
        """
        self.contextMenu.exec_(QtGui.QCursor.pos())

    def emitPathDoubleClicked(self, index):
        """
        Emit a signal when a path is double clicked.

        :param index:
        :return:
        """
        index = self.proxyModel.mapToSource(index)
        path = self.model.getPathFromIndex(index)
        self.pathDoubleClicked.emit(path)

    def emitSelectionChanged(self, selectedItem, deselectedItem):
        """
        Emit the selectionChanged signal.

        This method is connected to the view's selection model to that
        when a new row is selected, this signal emits the path of the
        newly selected item.
        """
        path = self.model.getPathFromItem(selectedItem)
        self.selectionChanged.emit(path)

    def expandAll(self):
        """
        Expand the tree
        """
        self.view.expandAll()

    def getCheckedPaths(self, leaf_only=True):
        """
        Return a list of paths that are currently checked

        @type leaf_only bool
        @param leaf_only If true, only items with no children will be returned
        """
        return self.model.getCheckedPaths(leaf_only)

    def getCheckState(self, path):
        """
        Get the checkstate of a path item

        @type path str
        @param path Path of the item

        @return QtCore.Qt.Checked or QtCore.Qt.Unchecked
        """
        return self.model.getCheckState(path)

    def getChildPaths(self, path, leaf_only=True):
        """
        Get a list of paths that are children of the given path

        @type path str
        @param path Parent path

        @return List of paths that are children of the given path
        """
        return self.model.getChildPaths(path, leaf_only)

    def getContextMenu(self):
        """
        Return the default context menu for the widget.  This is useful in sub-classes
        or other widgets which want to modify the context menu.

        @type return QMenu
        @return Context menu
        """
        return self.contextMenu

    def getData(self, skip_empty=True):
        """
        Get all the data from the model as a dictionary.

        @type skip_empty bool
        @param skip_empty Do not include paths that contain no data.

        @type return dictionary
        @return Dictionary of paths and values.
        """
        return self.model.getData(skip_empty)

    def getPaths(self):
        """
        Get all paths in the model

        @type return list
        @return List of strings of all paths in the widget
        """
        return self.model.getPaths()

    def getSelectedPath(self):
        """
        Return the path of the currently selected item.

        @type return str
        @return Path of the selected item
        """
        item = self.view.selectionModel().selection().indexes()[0]
        return self.model.getPathFromItem(item)

    def getTreeWidthHint(self):
        """
        Get a hint for resizing the tree width.

        @type return int
        @return Size hint as an int.
        """
        width = 0
        for col in range(self.model.columnCount()):
            width = width + self.view.sizeHintForColumn(col)
        return width

    def getUncheckedPaths(self, leaf_only=True):
        """
        Return a list of paths that are currently unchecked

        @type leaf_only bool
        @param leaf_only If true, only items with no children will be returned
        """
        return self.model.getCheckedPaths(leaf_only)

    def getValue(self, path, name=None):
        """
        Get the value of a path.  If columnName = None, the entire row is returned
        as a dictionary, where the keys are the columnNames

        @type path str
        @param path Path of the data to get

        @type return object
        @return The value of the column, or the entire row as a dictionary
        """
        return self.model.getValue(path, name)

    @QtCore.pyqtSlot(str)
    def filterTextChanged(self, pattern):
        """
        Slot that gets called when the filter textbox has changed

        @param text New text string used for filtering the model
        """
        self.proxyModel.setFilterRegExp(str(pattern))
        self.expandAll()

    @QtCore.pyqtSlot(str)
    def removePath(self, path):
        """
        Remove a path and all sub-paths

        @type str
        @param path Pathname to remove
        """
        self.model.removePath(path)

    @QtCore.pyqtSlot()
    def sizeHint(self):
        """
        Overload of the base sizeHint for the widget
        """
        size = self.view.sizeHint()
        size.setWidth(self.getTreeWidthHint())
        return size

    @QtCore.pyqtSlot(str,int)
    def setCheckState(self, path, checkState):
        if checkState != QtCore.Qt.Checked or QtCore.Qt.Unchecked:
            raise Exception('setCheckState must == QtCore.Qt.Checked OR QtCore.Qt.Unchecked')

        item = self.model.getItem(path)
        if item == None:
            raise Exception('Attempting to set checkstate for path "{}", but path does not exist'.format(path))

        item.setCheckState(checkState)
        if item.hasChildren():
            self._setChildrenCheckState(item, checkState)

    @QtCore.pyqtSlot(int)
    def setCheckStateAll(self, checkState):
        if checkState != QtCore.Qt.Checked or QtCore.Qt.Unchecked:
            raise Exception('setCheckState must == QtCore.Qt.Checked OR QtCore.Qt.Unchecked')

        for i in range(self.model.rowCount()):
            path = self.model.getPathFromItem(self.model.item(i))
            self.setCheckState(path, checkState)

    @QtCore.pyqtSlot(bool)
    def setFilterCaseSensitive(self, caseSensitive):
        """
        Set the filter case sensitivity

        @type caseSensitive bool
        @param caseSensitive Case sensitive when True or greater than zero
        """
        if caseSensitive:
            self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseSensitive)
        else:
            self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.expandAll()

    @QtCore.pyqtSlot(str, int)
    def setHeaderLabel(self, label, col=0):
        headerItem = self.model.horizontalHeaderItem(col)
        if headerItem == None:
            headerItem = QtGui.QStandardItem()
            self.model.setHorizontalHeaderItem(col,headerItem)
        headerItem.setData(label,QtCore.Qt.DisplayRole)

    @QtCore.pyqtSlot(list)
    def setHeaderLabels(self, labels):
        """
        Set the header of the widget
        @param labels List of strings to put in the widget's header
        """
        self.model.setHorizontalHeaderLabels(labels)

    def setModel(self, model):
        self.model = model
        self.model.pathValueChanged.connect(self.pathValueChanged)
        self.model.pathCheckChanged.connect(self.pathCheckChanged)
        self.proxyModel.setSourceModel(self.model)

    @QtCore.pyqtSlot(str, str, QtCore.QVariant, QtCore.QVariant)
    def setValue(self, path, columnName, value, format='str', color=None):
        """
        Set the value of a cell
        @param path Path of the cell
        @param columnName Column of the cell
        @param value value of the cell
        """
        self.model.setValue(path, columnName, value, format, color)

    def viewAll(self):
        """
        Expand the tree and adjust the columns
        """
        self.expandAll()
        self.adjustColumns()

    @QtCore.pyqtSlot(str, int)
    def _checkStateChanged(self, path, checkstate):
        """
        Connected to self.model.itemChanged.  This method will be called if an item is checked,
        which will in turn set the checkstate of it's children
        """
        item = self.model.getItem(str(path))
        for i in range(item.rowCount()):
            child = item.child(i)
            index = self.model.indexFromItem(child)
            if self.proxyModel.acceptRow(index):
                child.setCheckState(checkstate)
