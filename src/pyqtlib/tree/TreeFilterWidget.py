from pyqtlib import QtGui, QtCore

from pyqtlib import AnyChildFilterProxyModel
from pyqtlib.view import TreeView

class TreeFilterWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        # Proxy model, used for filtering
        self.proxyModel = AnyChildFilterProxyModel()
        self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self._view = TreeView()
        self._view.setModel(self.proxyModel)

        # Filter box and case toggle
        self.filterLineEdit = QtGui.QLineEdit()
        self.filterLineEdit.textChanged.connect(self.filterTextChanged)

        self.caseToggle = QtGui.QPushButton('Aa')
        self.caseToggle.setStyleSheet('padding: 3px 3px')  # set vertical and horizontal padding
        self.caseToggle.setCheckable(True)
        self.caseToggle.toggled.connect(self.setFilterCaseSensitive)
        self.caseToggle.setChecked(True)
        self.caseToggle.setToolTip('Toggle case sensitivity')

        # Widget layout
        self.filterLayout = QtGui.QHBoxLayout()
        self.filterLayout.addWidget(self.filterLineEdit)
        self.filterLayout.addWidget(self.caseToggle)

        self.layout = QtGui.QVBoxLayout()
        self.layout.addLayout(self.filterLayout)
        self.layout.addWidget(self._view)
        self.setLayout(self.layout)

    def view(self):
        return self._view

    def setView(self, view):
        item = self.layout.takeAt(1)
        item.widget().deleteLater()
        model = view.model()
        self.proxyModel.setSourceModel(model)
        self._view = view
        self._view.setModel(self.proxyModel)
        self.layout.insertWidget(1,self._view)

    def model(self):
        return self._model

    @QtCore.pyqtSlot(str)
    def filterTextChanged(self, pattern):
        """
        Slot that gets called when the filter textbox has changed

        @param text New text string used for filtering the model
        """
        self.proxyModel.setFilterRegExp(str(pattern))
        self._view.expandAll()

    def setModel(self, model):
        """
        Set the model for this view.

        Args:
            model: Mode as a subclass of the QAbstractItemModel
        """
        self._model = model
        self.proxyModel.setSourceModel(model)

    @QtCore.pyqtSlot(bool)
    def setFilterCaseSensitive(self, caseSensitive):
        """
        Set the filter case sensitivity

        @type caseSensitive bool
        @param caseSensitive Case sensitive when True or greater than zero
        """
        if caseSensitive:
            self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseSensitive)
        else:
            self.proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._view.expandAll()

    def setFilterRole(self, role):
        self.proxyModel.setFilterRole(role)

