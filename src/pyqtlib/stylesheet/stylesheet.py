
styleSheet = """
    QTreeView {
        alternate-background-color: rgb(CCCCCC);
        background : white;
        font-size: 12px;
        font-family: "Courier New";
    }
    QTableView {
        alternate-background-color: rgb(CCCCCC);
        background : white;
        font-size: 12px;
        font-family: "Courier New";
    }
"""

def getStyleSheet():
    return styleSheet