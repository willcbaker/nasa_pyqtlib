from pyqtlib import QtGui, QtCore

class ValueItem(QtGui.QStandardItem):
    """
    The ValueItem is a QStandardItem that allows for limited customized conversion of the output.
    In the constructor, the user can specify this item as one of a few types in order to
    format the output.  This allows users to format integers as hex, for example.  To set the value
    of the item, call the setData method with the role = ValueItem.DataRole.  Once constructed, this
    item will enforce strong typing of the underlying data both for setData and from user editing.
    """

    DataRole = QtCore.Qt.UserRole + 1

    # The posible conversion types
    conversion_map = {
        'hex16'   : lambda x : int(x,16),
        'hex32'   : lambda x : int(x,16),
        'hex64'   : lambda x : int(x,16),
        'int'   : lambda x : int(x, 0),
        'float' : lambda x : float(x),
        'str'   : lambda x : str(x)
    }

    # The formatting strings based on the format type
    format_string = {
        'hex16'  : '0x{:04X}',
        'hex32'  : '0x{:08X}',
        'hex64'  : '0x{:016X}',
        'int'  : '{}',
        'float': '{:< g}',
        'str'  : '{}'
    }

    def __init__(self, format='str'):
        """
        :param format: Format string used to display the data.  This can be one
                       of the keys in `ValueItem.format_string` or an arbitrary
                       format string, for example '{:.2f}'. See official Python
                       documentation on format strings for additional reference
        """
        super(ValueItem, self).__init__()
        try:
            self.fmtStr = ValueItem.format_string[format]
        except KeyError:
            self.fmtStr = format

        try:
            self.conv   = ValueItem.conversion_map[format]
        except KeyError:
            self.conv   = lambda x: x # no conversion

    def setData(self, value, role=QtCore.Qt.UserRole+1, *args, **kwargs):
        if role == ValueItem.DataRole:
            self.setText(self.fmtStr.format(value))

        elif role == QtCore.Qt.EditRole:
            value = self.conv(str(value))
            return self.setData(value, ValueItem.DataRole, *args, **kwargs)

        return super(ValueItem, self).setData(value, role, *args, **kwargs)
