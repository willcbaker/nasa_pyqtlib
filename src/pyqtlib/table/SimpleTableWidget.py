from pyqtlib import QtGui, QtCore

class SimpleTableModel(QtCore.QAbstractTableModel):
	def __init__(self,parent=None):
		super(SimpleTableModel, self).__init__(parent)
		self.modelData = {}

	def addValue(self, name, value='-'):
		self.modelData[name] = value
		self.layoutChanged.emit()

	def columnCount(self, parent):
		return 2

	def data(self, index, role=QtCore.Qt.DisplayRole):
		if role == QtCore.Qt.DisplayRole:
			keys = sorted(self.modelData.keys())
			if index.column() == 0:
				return keys[index.row()]
			elif index.column() == 1:
				k = keys[index.row()]
				val = self.modelData[k]
				formatStr = '{: > g}'
				try:
					rval = formatStr.format(float(val))
				except:
					rval = str(val)
				return rval
		elif role == QtCore.Qt.FontRole:
			return QtGui.QFont('TypeWriter',10)
		elif role == QtCore.Qt.TextAlignmentRole:
			return QtCore.Qt.AlignLeft + QtCore.Qt.AlignVCenter
		return None

	def getRowFromName(self, name):
		if name in self.modelData.keys():
			return sorted(self.modelData.keys()).index(name)

	def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
		if role == QtCore.Qt.DisplayRole:
			if orientation == QtCore.Qt.Horizontal:
				if section == 0:
					return 'name'
				elif section == 1:
					return 'value'
		return None

	def removeValue(self, name):
		if name in self.modelData.keys():
			del self.modelData[name]
			self.layoutChanged.emit()

	def rowCount(self, parent):
		return len(self.modelData.keys())

	def setValue(self,name, value):
		if name in self.modelData.keys():
			self.modelData[name] = value
			self.layoutChanged.emit()

class SimpleTableWidget(QtGui.QWidget):
	def __init__(self, parent=None):
		super(SimpleTableWidget, self).__init__(parent)
		
		self.tableView = QtGui.QTableView()
		self.tableModel = SimpleTableModel()
		self.tableView.setModel(self.tableModel)
		layout = QtGui.QVBoxLayout()
		layout.addWidget(self.tableView)
		self.setLayout(layout)
		self.show()

	def addValue(self,name):
		self.tableModel.addValue(name)
		self.tableView.update()

	def removeValue(self,name):
		self.tableModel.removeValue(name)

	def setValue(self,name,value):
		self.tableModel.setValue(name, value)
		self.tableView.update()

	def setValueVisible(self, name, visible=True):
		self.tableView.setRowHidden(self.tableModel.getRowFromName(name), not visible)
