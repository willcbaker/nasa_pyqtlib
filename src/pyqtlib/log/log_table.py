from pyqtlib import QtGui, QtCore


class LogModel(QtGui.QStandardItemModel):
    """
    StandardItem model for log data.  This class is mainly used to set the text
    and background color based on the log level.
    """

    LogLevelBackgroundColor = {
        'DEBUG': QtCore.Qt.green,
        'debug': QtCore.Qt.green,
        'INFO': QtCore.Qt.white,
        'info': QtCore.Qt.white,
        'NOTICE': QtCore.Qt.white,
        'notice': QtCore.Qt.white,
        'WARN': QtCore.Qt.yellow,
        'warn': QtCore.Qt.yellow,
        'WARNING': QtCore.Qt.yellow,
        'warning': QtCore.Qt.yellow,
        'ERROR': QtCore.Qt.red,
        'error': QtCore.Qt.red,
        'CRITICAL': QtCore.Qt.red,
        'critical': QtCore.Qt.red,
        'FATAL': QtCore.Qt.red,
        'fatal': QtCore.Qt.red
    }

    LogLevelTextColor = {
        'DEBUG': QtCore.Qt.black,
        'debug': QtCore.Qt.black,
        'INFO': QtCore.Qt.black,
        'info': QtCore.Qt.black,
        'NOTICE': QtCore.Qt.black,
        'notice': QtCore.Qt.black,
        'WARN': QtCore.Qt.black,
        'warn': QtCore.Qt.black,
        'WARNING': QtCore.Qt.black,
        'warning': QtCore.Qt.black,
        'ERROR': QtCore.Qt.white,
        'error': QtCore.Qt.white,
        'CRITICAL': QtCore.Qt.black,
        'critical': QtCore.Qt.black,
        'FATAL': QtCore.Qt.black,
        'fatal': QtCore.Qt.black
    }

    LogTimeIndex = 0
    LogHostIndex = 1
    LogSourceIndex = 2
    LogLevelIndex = 3
    LogCategoryIndex = 4
    LogMessageIndex = 5

    def __init__(self, parent=None):
        super(LogModel, self).__init__(parent)
        self.setHorizontalHeaderLabels(['time', 'host', 'source', 'level', 'category', 'message'])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        Overload the QStandardItemModel data method.
        """
        if role == QtCore.Qt.BackgroundRole:
            level = self.item(index.row(), LogModel.LogLevelIndex).text()
            try:
                color = LogModel.LogLevelBackgroundColor[str(level)]
            except KeyError:
                return None
            return QtGui.QBrush(color)

        if role == QtCore.Qt.ForegroundRole:
            level = self.item(index.row(), LogModel.LogLevelIndex).text()
            try:
                color = LogModel.LogLevelTextColor[str(level)]
            except:
                return None
            return QtGui.QBrush(color)

        if role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft

        return super(LogModel, self).data(index, role)


class LogTableWidget(QtGui.QWidget):
    LogLevels = {
        'DEBUG': 0,
        'debug': 0,
        'INFO': 10,
        'info': 10,
        'NOTICE': 20,
        'notice': 20,
        'WARN': 30,
        'warn': 30,
        'WARNING': 30,
        'warning': 30,
        'ERROR': 40,
        'error': 40,
        'CRITICAL': 50,
        'critical': 50,
        'ALERT': 60,
        'alert': 60,
        'FATAL': 70,
        'fatal': 70,
        'EMERGENCY': 80,
        'emergency': 80,
    }

    def __init__(self, parent=None):
        super(LogTableWidget, self).__init__(parent)

        self.filterLogLevel = 0

        self.layout = QtGui.QVBoxLayout()

        self.view = QtGui.QTableView()
        self.view.verticalHeader().setVisible(False)
        self.view.horizontalHeader().setStretchLastSection(True)

        self.sourceModel = LogModel()
        self.proxyModel = QtGui.QSortFilterProxyModel()
        self.proxyModel.setSourceModel(self.sourceModel)
        self.proxyModel.setDynamicSortFilter(True)

        # Action to enable/disable auto-scrolling of the view window
        self.setLiveUpdateAction = QtGui.QAction('&Live Update', self)
        self.setLiveUpdateAction.setCheckable(True)
        self.setLiveUpdateAction.setChecked(True)

        self.logLevelSignalMapper = QtCore.QSignalMapper()

        # setLogLevel actions
        self.setLogLevelDebugAction = QtGui.QAction('&Debug', self, triggered=self.logLevelSignalMapper.map)
        self.setLogLevelInfoAction = QtGui.QAction('&Info', self, triggered=self.logLevelSignalMapper.map)
        self.setLogLevelNoticeAction = QtGui.QAction('&Notice', self, triggered=self.logLevelSignalMapper.map)
        self.setLogLevelWarningAction = QtGui.QAction('&Warning', self, triggered=self.logLevelSignalMapper.map)
        self.setLogLevelErrorAction = QtGui.QAction('&Error', self, triggered=self.logLevelSignalMapper.map)

        self.showUnmatchedLevelsAction = QtGui.QAction('Show unmatched', self)
        self.showUnmatchedLevelsAction.setCheckable(True)
        self.showUnmatchedLevelsAction.setChecked(False)
        self.showUnmatchedLevelsAction.triggered.connect(self.showUnmatchedLevelsCheckChanged)

        self.setLogLevelActionGroup = QtGui.QActionGroup(self)
        self.setLogLevelActionGroup.setExclusive(True)
        self.setLogLevelActionGroup.addAction(self.setLogLevelDebugAction)
        self.setLogLevelActionGroup.addAction(self.setLogLevelInfoAction)
        self.setLogLevelActionGroup.addAction(self.setLogLevelNoticeAction)
        self.setLogLevelActionGroup.addAction(self.setLogLevelWarningAction)
        self.setLogLevelActionGroup.addAction(self.setLogLevelErrorAction)

        for action in self.setLogLevelActionGroup.actions():
            action.setCheckable(True)

        self.setLogLevelInfoAction.setChecked(True)

        self.resizeColumnsToContentsAction = QtGui.QAction('&Resize Columns', self)
        self.resizeColumnsToContentsAction.triggered.connect(self.resizeColumnsToContents)

        # setLogLevelMenu
        self.setLogLevelMenu = QtGui.QMenu('Set Log Level')
        self.setLogLevelMenu.addActions(self.setLogLevelActionGroup.actions())

        # Map setLogLevel action signals
        self.logLevelSignalMapper.setMapping(self.setLogLevelDebugAction, LogTableWidget.LogLevels['DEBUG'])
        self.logLevelSignalMapper.setMapping(self.setLogLevelInfoAction, LogTableWidget.LogLevels['INFO'])
        self.logLevelSignalMapper.setMapping(self.setLogLevelNoticeAction, LogTableWidget.LogLevels['NOTICE'])
        self.logLevelSignalMapper.setMapping(self.setLogLevelWarningAction, LogTableWidget.LogLevels['WARNING'])
        self.logLevelSignalMapper.setMapping(self.setLogLevelErrorAction, LogTableWidget.LogLevels['ERROR'])
        self.logLevelSignalMapper.mapped['int'].connect(self.setFilterLogLevel)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showContextMenu)

        self.view.setModel(self.proxyModel)
        self.view.setSortingEnabled(True)

        # this will make the time column fit fairly well
        self.view.setColumnWidth(0, 150)

        self.layout.addWidget(self.view)
        self.setLayout(self.layout)

    def addEntry(self, timestamp, host, source, level, category, message):
        """
        Add an entry to the log table

        Args:
            timestamp: Timestamp of the log entry as a string
            host: Hostname as a string
            source: Where the log entry was generated as a string
            level: Severity as a string
            category: Logging category as a string
            message: Message as a string
        """
        self.sourceModel.appendRow(
            [QtGui.QStandardItem(timestamp),
             QtGui.QStandardItem(host),
             QtGui.QStandardItem(source),
             QtGui.QStandardItem(level),
             QtGui.QStandardItem(category),
             QtGui.QStandardItem(message)])
        if self.setLiveUpdateAction.isChecked():
            self.view.sortByColumn(LogModel.LogTimeIndex, QtCore.Qt.AscendingOrder)
            self.view.scrollToBottom()
        self.view.resizeRowsToContents()

    def addEntries(self, entries):
        """
        Add a list of entries to the log window

        Args:
            entries: List of tuples of the form (timestamp, hostname, source, level, category, message)
        """
        self.proxyModel.setDynamicSortFilter(False)
        for timestamp, host, source, level, category, message in entries:
            self.sourceModel.appendRow(
                [QtGui.QStandardItem(timestamp),
                 QtGui.QStandardItem(host),
                 QtGui.QStandardItem(source),
                 QtGui.QStandardItem(level),
                 QtGui.QStandardItem(category),
                 QtGui.QStandardItem(message)])
        if self.setLiveUpdateAction.isChecked():
            self.view.sortByColumn(LogModel.LogTimeIndex, QtCore.Qt.AscendingOrder)
            self.view.scrollToBottom()
        self.view.resizeRowsToContents()
        self.proxyModel.setDynamicSortFilter(True)

    def contextMenuEvent(self, event):
        menu = QtGui.QMenu()
        menu.addActions(self.setLogLevelMenu.actions())
        menu.addSeparator()
        menu.addAction(self.showUnmatchedLevelsAction)
        menu.addAction(self.setLiveUpdateAction)
        menu.addAction(self.resizeColumnsToContentsAction)
        menu.exec_(QtGui.QCursor.pos())

    def getFilterRegex(self, filterLogLevel, showUnmatchedLevels):
        """
        Calculate the regex based on the current filterLogLevel, and the showUnmatchedLevels

        Returns:
            regular expression as a string
        """
        showStrings = []
        filterStrings = []
        for levelString, value in LogTableWidget.LogLevels.items():
            if value >= filterLogLevel:
                showStrings.append(levelString)
            else:
                filterStrings.append(levelString+'\s*$')

        if showUnmatchedLevels:
            if len(filterStrings) == 0:
                regex = '.*'
            else:
                allFilterStrings = '|'.join(filterStrings)
                # In order for the filter proxy to match properly, the negative lookahead needs to match the beginning
                # of the string.
                regex = '^(?!{})'.format(allFilterStrings)
        else:
            allShowStrings = '|'.join(showStrings)
            regex = allShowStrings
        return regex

    def resizeColumnsToContents(self):
        self.view.resizeRowsToContents()
        self.view.resizeColumnsToContents()
        self.view.horizontalHeader().setStretchLastSection(True)

    def setFilterLogLevel(self, level):
        """
        Set the table's log-level filter

        Args:
            level: minimum log level to display as an int
        """
        self.filterLogLevel = level
        regex = self.getFilterRegex(self.filterLogLevel, self.showUnmatchedLevelsAction.isChecked())
        self.proxyModel.setFilterRegExp(regex)
        self.proxyModel.setFilterKeyColumn(LogModel.LogLevelIndex)

    def showUnmatchedLevelsCheckChanged(self):
        """
        Slot connected to showUnmatchedLevelsAction.  This will cause the filter to get recalculated.
        """
        self.setFilterLogLevel(self.filterLogLevel)

    def showContextMenu(self, event):
        '''
        Create and show a custom context menu.  This event is connected to both
        the PlainTextEdit and overall widget's customContextMenuRequested signal
        '''
        self.contextMenuEvent(event)
