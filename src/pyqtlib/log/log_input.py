from pyqtlib import QtGui, QtCore
from nasa_common_logging import log_level

import logging

class LogInputWidget(QtGui.QWidget):

    combobox_log_levels = ['DEBUG','INFO','WARNING','ERROR']

    def __init__(self, loggerName=None, parent=None):
        QtGui.QWidget.__init__(self, parent=parent)
        if loggerName:
            self.logger = logging.getLogger(loggerName)
        else:
            self.logger = logging.getLogger()

        self.level = None
        self.setLogLevel('INFO')

        self.textBox = QtGui.QLineEdit()
        self.textBox.returnPressed.connect(self.sendButtonPressed)

        self.logLevelSelect = QtGui.QComboBox()
        self.logLevelSelect.addItems(LogInputWidget.combobox_log_levels)
        self.logLevelSelect.currentIndexChanged.connect(self.setLogLevel)
        self.logLevelSelect.setCurrentIndex(1)

        self.sendButton = QtGui.QPushButton('log')
        self.sendButton.clicked.connect(self.sendButtonPressed)

        layout = QtGui.QHBoxLayout(self)
        layout.addWidget(self.logLevelSelect)
        layout.addWidget(self.sendButton)
        layout.addWidget(self.textBox)
        self.setLayout(layout)

    @QtCore.pyqtSlot(str)
    def setLogLevel(self, level):
        """
        Set the log level of this widget by string.

        The available strings are defined in nasa_common_logging's log_level dictionary

        Args:
            level: log level as a string
        """
        if isinstance(level, int):
            level = LogInputWidget.combobox_log_levels[level]
        if level in log_level.keys():
            self.level = log_level[level]
        else:
            raise ValueError('Attempting to set log level to invalid value: "{}"'.format(level))

    def logMessage(self, message):
        self.logger.log(self.level, str(message))

    def sendButtonPressed(self):
        message = self.textBox.text()
        self.logMessage(message)

