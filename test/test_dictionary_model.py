import unittest

from pyqtlib import QtCore, QtGui
from pyqtlib.model import DictionaryModel, DictionaryModelItem


class TestDictionaryModel(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_getValue(self):
        model = DictionaryModel()
        d= {'a':1, 'b':2, 'c':[1,2,3]}
        model.setDictionary(d)

        self.assertEqual(1, model.getValue(['a']))
        self.assertEqual(3, model.getValue(['c',2]))
        self.assertEqual([1,2,3], model.getValue(['c']))
        self.assertEqual(d, model.getDictionary())

        self.assertRaises(KeyError, model.getValue, ['foo'])

    #@unittest.skip("reasons")
    def test_setValue(self):
        model = DictionaryModel()
        d= {'a':1, 'b':2, 'c':[1,2,3]}
        model.setDictionary(d)

        model.setValue(['c',2],5)
        dictionary = model.getDictionary()
        self.assertEqual(5, model.getDictionary()['c'][2])

        model.setValue(['d'],['tree','dog','bat'])
        self.assertEqual(['tree','dog','bat'],model.getValue(['d']))

        model.setValue(['d',1], [9,8,7])
        self.assertEqual(9, model.getValue(['d',1,0]))

        model.setValue(['c'],True)
        self.assertEqual(True, model.getValue(['c']))

    def test_manuallyEdit(self):
        model = DictionaryModel()

        d= {'a':1, 'c':[1,2,3], 'b':2}
        model.setDictionary(d)

        root = model.invisibleRootItem()
        self.assertEqual('a', root.child(0,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(1,   root.child(0,1).data(QtCore.Qt.DisplayRole))

        self.assertEqual('b', root.child(1,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(2,   root.child(1,1).data(QtCore.Qt.DisplayRole))

        self.assertEqual('c', root.child(2,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(0,   root.child(2,0).child(0,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(1,   root.child(2,0).child(0,1).data(QtCore.Qt.DisplayRole))

        self.assertEqual(1,   root.child(2,0).child(1,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(2,   root.child(2,0).child(1,1).data(QtCore.Qt.DisplayRole))

        root.child(2,1).setData("{'a':42, 'b':180}", QtCore.Qt.DisplayRole)


        self.assertEqual("{'a':42, 'b':180}", root.child(2,1).data(QtCore.Qt.EditRole))
        model.refresh()

        root = model.invisibleRootItem()
        self.assertEqual('a', root.child(0,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(1, root.child(0,1).data(QtCore.Qt.DisplayRole))

        self.assertEqual('b', root.child(1,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(2,   root.child(1,1).data(QtCore.Qt.DisplayRole))

        self.assertEqual('c', root.child(2,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(None, root.child(2,1).data(QtCore.Qt.DisplayRole))

        #self.assertEqual('a', root.child(2,0).child(0,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(42,   root.child(2,0).child(0,1).data(QtCore.Qt.DisplayRole))
        #self.assertEqual('b', root.child(2,0).child(1,0).data(QtCore.Qt.DisplayRole))
        self.assertEqual(180,   root.child(2,0).child(1,1).data(QtCore.Qt.DisplayRole))

    def test_expanded(self):
        model = DictionaryModel()

        d = {'a': 'cat',
             'b': [1,
                   2,
                   3],
             'c': True
             }

        model.setDictionary(d)
        model.cacheExpandedStates()
        states = model.getExpandedStates()
        self.assertFalse(states['a'][0])
        self.assertFalse(states['b'][0])
        self.assertFalse(states['c'][0])

        idx = model.index(1, 0)
        self.assertEqual('b', model.data(idx, QtCore.Qt.DisplayRole))

        self.assertFalse(model.data(idx, DictionaryModelItem.ExpandedRole))

        model.setItemExpanded(idx)
        self.assertTrue(model.data(idx, DictionaryModelItem.ExpandedRole))

        model.setItemCollapsed(idx)
        self.assertFalse(model.data(idx, DictionaryModelItem.ExpandedRole))
        
    def test_add_redundant(self):
        model = DictionaryModel()
        model.addItem("item", 1)
        model.addItem("item", 2)
        model.addItem("item", "item<3>")
        model.addItem("item", 2)
        
        data = model.getDictionary()
        self.assertTrue("item" in data)
        self.assertTrue(data["item"], 1)
        self.assertTrue("item<1>" in data)
        self.assertTrue(data["item<1>"], 2)
        self.assertTrue("item<2>" in data)
        self.assertTrue(data["item<2>"], "item<3>")
        self.assertTrue("item<3>" in data)
        self.assertTrue(data["item<3>"], 2)

if __name__ == "__main__":
    unittest.main()
