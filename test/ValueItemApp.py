from pyqtlib import QtCore, QtGui
from pyqtlib import ValueItem

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.model = QtGui.QStandardItemModel()
        self.view = QtGui.QTreeView()

        self.view.setModel(self.model)

        self.setup(self.model)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timeUp)
        self.timer.start(1000)

        self.setCentralWidget(self.view)
        self.show()

    def setup(self, model):

        self.item1 = ValueItem(format='float')
        self.item2 = ValueItem(format='hex64')
        self.item3 = ValueItem(format='int')
        self.item4 = ValueItem(format='str')

        self.item1.setData(-1.234, )
        self.item2.setData(0xA001,ValueItem.DataRole)
        self.item3.setData(12345,ValueItem.DataRole)
        self.item4.setData('splat',ValueItem.DataRole)

        self.model.appendRow([self.item1,self.item2, self.item3, self.item4])

    def timeUp(self):
        a = self.model.item(0,0).data(ValueItem.DataRole)
        b = self.model.item(0,1).data(ValueItem.DataRole)
        c = self.model.item(0,2).data(ValueItem.DataRole)
        d = self.model.item(0,3).data(ValueItem.DataRole)

        typename = lambda x : type(x).__name__
        print 'a({})={} b({})={} c({})={} d({})={}'.format(typename(a), a, typename(b), b, typename(c), c, typename(d), d)

def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()