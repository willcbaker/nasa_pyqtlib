import unittest

from pyqtlib import QtGui, QtCore

from pyqtlib.view.TreeView import TreeView


class TestTreeView(unittest.TestCase):

    def setUp(self):
        self.app = QtGui.QApplication([])

    def tearDown(self):
        pass

    def test_getExpandedState_one_row_no_children(self):
        model = QtGui.QStandardItemModel()

        nameItem = QtGui.QStandardItem('name')

        model.appendRow([nameItem])

        view = TreeView()
        view.setModel(model)

        state = view.getExpandedStates()
        self.assertFalse(state.values()[0])

    def test_getExpandedState_one_row_with_children(self):
        model = QtGui.QStandardItemModel()

        parentItem = QtGui.QStandardItem('parent')
        parentItem.appendRow([QtGui.QStandardItem('child1')])
        parentItem.appendRow([QtGui.QStandardItem('child2')])
        parentItem.appendRow([QtGui.QStandardItem('child3')])

        model.appendRow([parentItem])
        idx = model.index(0, 0)

        view = TreeView()
        view.setModel(model)

        state = view.getExpandedStates()
        self.assertFalse(state[idx])

        view.setExpanded(idx, True)

        state = view.getExpandedStates()
        self.assertTrue(state[idx])

        # view.show()
        # self.app.exec_()

    def test_setExpandedState(self):
        model = QtGui.QStandardItemModel()

        parentItem = QtGui.QStandardItem('parent')
        parentItem.appendRow([QtGui.QStandardItem('child1')])
        parentItem.appendRow([QtGui.QStandardItem('child2')])
        parentItem.appendRow([QtGui.QStandardItem('child3')])

        model.appendRow([parentItem])
        idx = model.index(0, 0)

        view = TreeView()
        view.setModel(model)

        state1 = view.getExpandedStates()
        self.assertFalse(state1[idx])

        view.setExpanded(idx, True)

        state2 = view.getExpandedStates()
        self.assertTrue(state2[idx])
        self.assertTrue(view.isExpanded(idx))

        view.setExpandedStates(state1)
        self.assertFalse(view.isExpanded(idx))


if __name__ == "__main__":
    unittest.main()

