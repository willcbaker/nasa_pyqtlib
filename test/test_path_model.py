import unittest

from pyqtlib import QtGui, QtCore

from pyqtlib import PathModel
from pyqtlib import path_model


class TestPathToList(unittest.TestCase):

    def test_pathToList_default(self):
        path = '/path/to/data'
        delimiters = '/'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, ['/path','/to','/data'])

    def test_pathToList_consecutiveDelimiters(self):
        path = '///path//to/data'
        delimiters = '/'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, ['/','/','/path','/','/to','/data'])

    def test_pathToList_multipleDelimiters(self):
        path = '/path.to.data'
        delimiters = '/.'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, ['/path','.to','.data'])

    def test_pathToList_multipleConsecutiveDelimiters(self):
        path = '/path./to.data'
        delimiters = '/.'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, ['/path','.','/to','.data'])

    def test_pathToList_noLeadingDelimiters(self):
        path = 'path.to.data'
        delimiters = '.'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, ['path','.to','.data'])

    def test_pathToList_noMatchingDelimiters(self):
        path = 'path.to.data'
        delimiters = '?'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, [path])

    def test_pathToList_matchOnlyLeadingDelimiters(self):
        path = '?path.to.data'
        delimiters = '?'

        pathList = path_model.pathToList(path, delimiters)
        self.assertEqual(pathList, [path])

class TestPathModel(unittest.TestCase):

    def test__createPathItem(self):
        model = PathModel(checkable=True)

        path = '/usr/local/lib'
        name = 'lib'

        item = model._createPathItem(path, name)
        self.assertEqual(item.data(model.PathRole),path)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole),name)

        # verify not editable and is checkable
        self.assertEqual(item.isEditable(), False)
        self.assertEqual(item.isCheckable(), True)

    #@unittest.skip('reasons...')
    def test_addPath(self):
        model = PathModel()
        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')

        self.assertEqual(model.columnCount(),1)
        self.assertTrue('/usr/local/bin' in model.pathItems)
        self.assertEqual(model.modelValues['/usr/local/bin'], {})
        self.assertTrue('a' in model.modelValues)
        self.assertEqual(model.modelValues['a'], {})

        self.assertEqual(item1.data(PathModel.PathRole),'/usr/local/bin')
        self.assertEqual(item1.data(QtCore.Qt.DisplayRole),'/bin')

        self.assertEqual(item2.data(PathModel.PathRole),'a')
        self.assertEqual(item2.data(QtCore.Qt.DisplayRole),'a')

        self.assertEqual(model.addPath('a'),None)

    def test_clearAll(self):
        model = PathModel()
        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')
        self.assertEqual(model.columnCount(),1)
        self.assertEqual(model.rowCount(),2)
        self.assertEqual(len(model.pathItems),4)
        model.clearAll()
        self.assertEqual(model.columnCount(),0)
        self.assertEqual(model.rowCount(),0)
        self.assertEqual(model.pathItems,{})

    def test_clearData(self):
        model = PathModel()
        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')
        self.assertEqual(model.columnCount(),1)
        self.assertEqual(model.rowCount(),2)
        self.assertEqual(len(model.pathItems),4)
        model.clearData()
        self.assertEqual(model.columnCount(),1)
        self.assertEqual(model.rowCount(),0)
        self.assertEqual(len(model.pathItems),0)

    def test_getCheckedPaths(self):
        model = PathModel()

        paths = model.getCheckedPaths(leaf_only=False)
        self.assertEqual(len(paths),0)

        paths = model.getCheckedPaths()
        self.assertEqual(len(paths),0)

        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')
        for item in model.pathItems.values():
            item.setCheckState(QtCore.Qt.Checked)

        paths = model.getCheckedPaths()
        self.assertEqual(len(paths),2)
        self.assertEqual(sorted(paths),sorted(['/usr/local/bin','a']))

        paths = model.getCheckedPaths(leaf_only=False)
        self.assertEqual(len(paths),4)
        self.assertEqual(sorted(paths),sorted(['/usr','/usr/local','/usr/local/bin','a']))

    def test_getCheckState(self):
        model = PathModel()
        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')

        item2.setCheckState(QtCore.Qt.Checked)

        self.assertEqual(model.getCheckState('a'), QtCore.Qt.Checked)
        self.assertEqual(model.getCheckState('/usr/local/bin'), QtCore.Qt.Unchecked)
        self.assertRaises(Exception, model.getCheckState, 'bogus')

    def test_getChildPaths(self):
        model = PathModel()

        model.addPath('/usr/local/bin')
        model.addPath('/usr/local/script/myscript')
        model.addPath('/usr/home/other')

        #leaf only == true
        paths = model.getChildPaths('/usr/local')

        self.assertTrue('/usr/local/bin' in paths)
        self.assertTrue('/usr/local/script/myscript' in paths)
        self.assertEqual(2,len(paths))

        paths = model.getChildPaths('/usr/local',leaf_only=False)
        self.assertTrue('/usr/local/bin' in paths)
        self.assertTrue('/usr/local/script' in paths)
        self.assertTrue('/usr/local/script/myscript' in paths)
        self.assertEqual(3,len(paths))

    def test_getData(self):
        model = PathModel()

        model.setValue('/usr/local/int','a',1,format='int')
        model.setValue('/usr/local/int','b',2,format='int')
        model.setValue('/usr/local/int','c',3,format='int')

        model.setValue('/usr/bin/string','a','foo',format='str')
        model.setValue('/usr/bin/string','b','bar',format='str')
        model.setValue('/usr/bin/string','c','baz',format='str')

        model.setValue('/usr/local/float','a',-1.234,format='float')
        model.setValue('/usr/local/float','b',1.234,format='float')
        model.setValue('/usr/local/float','c',9.876,format='float')


        answerSkipEmpty = {}
        answerSkipEmpty['/usr/local/int'] = { 'a':1, 'b':2, 'c': 3}
        answerSkipEmpty['/usr/bin/string'] = { 'a':'foo', 'b':'bar', 'c': 'baz'}
        answerSkipEmpty['/usr/local/float'] = {'a':-1.234, 'b': 1.234, 'c': 9.876}
        self.assertEqual(model.getData(), answerSkipEmpty)

        answerFull = answerSkipEmpty
        answerFull['/usr'] = {}
        answerFull['/usr/local'] = {}
        answerFull['/usr/bin'] = {}
        self.assertEqual(model.getData(skip_empty=False), answerFull)

    def test_getPathFromItem(self):
        model = PathModel()

        model.setValue('/usr/local/bin','a',0xAA01,format='hex16')
        model.setValue('/usr/local/bin','b','2',format='str')
        model.setValue('/usr/local/bin','c',3,format='int')

        item_a = model.modelValues['/usr/local/bin']['a']
        item_b = model.modelValues['/usr/local/bin']['b']
        item_c = model.modelValues['/usr/local/bin']['c']

        self.assertEqual(model.getPathFromItem(item_a),'/usr/local/bin')
        self.assertEqual(model.getPathFromItem(item_b),'/usr/local/bin')
        self.assertEqual(model.getPathFromItem(item_c),'/usr/local/bin')

        pathItem = model.pathItems['/usr/local/bin']
        self.assertEqual(model.getPathFromItem(pathItem),'/usr/local/bin')

    def test_getPathFromIndex(self):
        model = PathModel()
        model.setValue('/usr/local/bin','a',0xAA01,format='hex16')

        valueItem = model.modelValues['/usr/local/bin']['a']
        valueItemIndex = model.indexFromItem(valueItem)
        self.assertEqual(model.getPathFromIndex(valueItemIndex),'/usr/local/bin')

        pathItem = model.pathItems['/usr/local/bin']
        pathItemIndex = model.indexFromItem(pathItem)
        self.assertEqual(model.getPathFromIndex(pathItemIndex),'/usr/local/bin')

    #@unittest.skip('reasons...')
    def test_getSetValue(self):
        model = PathModel()

        model.setValue('/usr/local/bin','a',0xAA01,format='hex16')
        model.setValue('/usr/local/bin','b','2',format='str')
        model.setValue('/usr/local/bin','c',3,format='int')

        self.assertEqual(model.getValue('/usr/local/bin','a'),0xAA01)
        self.assertEqual(model.getValue('/usr/local/bin','b'),'2')
        self.assertEqual(model.getValue('/usr/local/bin','c'),3)

        item_a = model.modelValues['/usr/local/bin']['a']
        item_b = model.modelValues['/usr/local/bin']['b']
        item_c = model.modelValues['/usr/local/bin']['c']

        self.assertEqual(item_a.data(PathModel.PathRole),'/usr/local/bin')
        self.assertEqual(item_b.data(PathModel.PathRole),'/usr/local/bin')
        self.assertEqual(item_c.data(PathModel.PathRole),'/usr/local/bin')

    def test_removePath(self):
        model = PathModel()

        item1 = model.addPath('/usr/local/bin')
        item2 = model.addPath('a')
        self.assertEqual(4, len(model.getPaths()))

        model.removePath('/usr/local/bin')
        self.assertEqual(3, len(model.getPaths()))

        model.removePath('/usr')
        self.assertEqual(1, len(model.getPaths()))

        model.removePath('notInThere')






if __name__ == "__main__":
    unittest.main()